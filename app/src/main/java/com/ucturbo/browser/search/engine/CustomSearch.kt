package com.ucturbo.browser.search.engine

import com.ucturbo.browser.R

/**
 * A custom search engine.
 */
class CustomSearch(queryUrl: String) : BaseSearchEngine(
    "file:///android_asset/styx.png",
    queryUrl,
    R.string.search_engine_custom
)
