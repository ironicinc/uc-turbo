package com.ucturbo.browser.search.engine

import com.ucturbo.browser.R

/**
 * The Ask search engine.
 */
class AskSearch : BaseSearchEngine(
    "file:///android_asset/ask.png",
    "http://www.ask.com/web?qsrc=0&o=0&l=dir&qo=StyxBrowser&q=",
    R.string.search_engine_ask
)
