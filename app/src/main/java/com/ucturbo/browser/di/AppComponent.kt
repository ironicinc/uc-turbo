package com.ucturbo.browser.di

import com.ucturbo.browser.BrowserApp
import com.ucturbo.browser.adblock.BloomFilterAdBlocker
import com.ucturbo.browser.adblock.NoOpAdBlocker
import com.ucturbo.browser.browser.BrowserPopupMenu
import com.ucturbo.browser.browser.SearchBoxModel
import com.ucturbo.browser.browser.activity.BrowserActivity
import com.ucturbo.browser.browser.activity.ThemableBrowserActivity
import com.ucturbo.browser.browser.bookmarks.BookmarksDrawerView
import com.ucturbo.browser.device.BuildInfo
import com.ucturbo.browser.dialog.StyxDialogBuilder
import com.ucturbo.browser.download.StyxDownloadListener
import com.ucturbo.browser.reading.activity.ReadingActivity
import com.ucturbo.browser.search.SuggestionsAdapter
import com.ucturbo.browser.settings.activity.SettingsActivity
import com.ucturbo.browser.settings.activity.ThemableSettingsActivity
import com.ucturbo.browser.settings.fragment.*
import com.ucturbo.browser.view.StyxChromeClient
import com.ucturbo.browser.view.StyxView
import com.ucturbo.browser.view.StyxWebClient
import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (AppBindsModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun buildInfo(buildInfo: BuildInfo): Builder

        fun build(): AppComponent
    }

    fun inject(activity: BrowserActivity)

    fun inject(fragment: BookmarkSettingsFragment)

    fun inject(builder: StyxDialogBuilder)

    fun inject(styxView: StyxView)

    fun inject(activity: ThemableBrowserActivity)

    fun inject(app: BrowserApp)

    fun inject(activity: ReadingActivity)

    fun inject(webClient: StyxWebClient)

    fun inject(activity: SettingsActivity)

    fun inject(activity: ThemableSettingsActivity)

    fun inject(listener: StyxDownloadListener)

    fun inject(fragment: PrivacySettingsFragment)

    fun inject(suggestionsAdapter: SuggestionsAdapter)

    fun inject(chromeClient: StyxChromeClient)

    fun inject(searchBoxModel: SearchBoxModel)

    fun inject(generalSettingsFragment: GeneralSettingsFragment)

    fun inject(displaySettingsFragment: DisplaySettingsFragment)

    fun inject(adBlockSettingsFragment: AdBlockSettingsFragment)

    fun inject(bookmarksView: BookmarksDrawerView)

    fun inject(popupMenu: BrowserPopupMenu)

    fun inject(appsSettingsFragment: AppsSettingsFragment)

    fun provideBloomFilterAdBlocker(): BloomFilterAdBlocker

    fun provideNoOpAdBlocker(): NoOpAdBlocker

}
