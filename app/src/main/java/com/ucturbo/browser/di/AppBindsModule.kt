package com.ucturbo.browser.di

import com.ucturbo.browser.adblock.allowlist.AllowListModel
import com.ucturbo.browser.adblock.allowlist.SessionAllowListModel
import com.ucturbo.browser.adblock.source.AssetsHostsDataSource
import com.ucturbo.browser.adblock.source.HostsDataSource
import com.ucturbo.browser.adblock.source.HostsDataSourceProvider
import com.ucturbo.browser.adblock.source.PreferencesHostsDataSourceProvider
import com.ucturbo.browser.browser.cleanup.DelegatingExitCleanup
import com.ucturbo.browser.browser.cleanup.ExitCleanup
import com.ucturbo.browser.database.adblock.HostsDatabase
import com.ucturbo.browser.database.adblock.HostsRepository
import com.ucturbo.browser.database.allowlist.AdBlockAllowListDatabase
import com.ucturbo.browser.database.allowlist.AdBlockAllowListRepository
import com.ucturbo.browser.database.bookmark.BookmarkDatabase
import com.ucturbo.browser.database.bookmark.BookmarkRepository
import com.ucturbo.browser.database.downloads.DownloadsDatabase
import com.ucturbo.browser.database.downloads.DownloadsRepository
import com.ucturbo.browser.database.history.HistoryDatabase
import com.ucturbo.browser.database.history.HistoryRepository
import com.ucturbo.browser.ssl.SessionSslWarningPreferences
import com.ucturbo.browser.ssl.SslWarningPreferences
import dagger.Binds
import dagger.Module

/**
 * Dependency injection module used to bind implementations to interfaces.
 */
@Module
interface AppBindsModule {

    @Binds
    fun bindsExitCleanup(delegatingExitCleanup: DelegatingExitCleanup): ExitCleanup

    @Binds
    fun bindsBookmarkModel(bookmarkDatabase: BookmarkDatabase): BookmarkRepository

    @Binds
    fun bindsDownloadsModel(downloadsDatabase: DownloadsDatabase): DownloadsRepository

    @Binds
    fun bindsHistoryModel(historyDatabase: HistoryDatabase): HistoryRepository

    @Binds
    fun bindsAdBlockAllowListModel(adBlockAllowListDatabase: AdBlockAllowListDatabase): AdBlockAllowListRepository

    @Binds
    fun bindsAllowListModel(sessionAllowListModel: SessionAllowListModel): AllowListModel

    @Binds
    fun bindsSslWarningPreferences(sessionSslWarningPreferences: SessionSslWarningPreferences): SslWarningPreferences

    @Binds
    fun bindsHostsDataSource(assetsHostsDataSource: AssetsHostsDataSource): HostsDataSource

    @Binds
    fun bindsHostsRepository(hostsDatabase: HostsDatabase): HostsRepository

    @Binds
    fun bindsHostsDataSourceProvider(preferencesHostsDataSourceProvider: PreferencesHostsDataSourceProvider): HostsDataSourceProvider
}
